const solution = function (A) {
  const S = new Set(A)
  return S.size
}

const A = [2, 1, 1, 2, 3, 1]
console.log(solution(A))
