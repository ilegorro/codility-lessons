const fibonacci = function(n, numbers) {
  numbers = numbers || {}
  if (n === 0) {
    return 0
  } else if (n === 1) {
    return 1
  }
  if (numbers[n]) {
    return numbers[n]
  }
  return (numbers[n] = fibonacci(n - 1, numbers) + fibonacci(n - 2, numbers))
}

const number = +process.argv[2]
if (number) {
  console.log(fibonacci(number))
} else {
  console.log(`Incorrect input: ${number}`)
}