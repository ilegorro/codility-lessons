# Codility lessons

These are my [Codility lessons](https://app.codility.com/programmers/lessons/) solutions.

### Lesson 1 - Iterations

|                                        Task                                        |            My solution            |
| :--------------------------------------------------------------------------------: | :-------------------------------: |
| [BinaryGap](https://app.codility.com/programmers/lessons/1-iterations/binary_gap/) | [Link](lesson1-BinaryGap/test.js) |

### Lesson 2 - Arrays

|                                                   Task                                                   |                  My solution                  |
| :------------------------------------------------------------------------------------------------------: | :-------------------------------------------: |
|         [CyclicRotation](https://app.codility.com/programmers/lessons/2-arrays/cyclic_rotation/)         |    [Link](lesson2-CyclicRotation/test.js)     |
| [OddOccurrencesInArray](https://app.codility.com/programmers/lessons/2-arrays/odd_occurrences_in_array/) | [Link](lesson2-OddOccurrencesInArray/test.js) |

### Lesson 3 - Time Complexity

|                                                 Task                                                 |               My solution               |
| :--------------------------------------------------------------------------------------------------: | :-------------------------------------: |
|         [FrogJmp](https://app.codility.com/programmers/lessons/3-time_complexity/frog_jmp/)          |     [Link](lesson3-FrogJmp/test.js)     |
| [PermMissingElem](https://app.codility.com/programmers/lessons/3-time_complexity/perm_missing_elem/) | [Link](lesson3-PermMissingElem/test.js) |
| [TapeEquilibrium](https://app.codility.com/programmers/lessons/3-time_complexity/tape_equilibrium/)  | [Link](lesson3-TapeEquilibrium/test.js) |

### Lesson 4 - Counting Elements

|                                                Task                                                 |              My solution               |
| :-------------------------------------------------------------------------------------------------: | :------------------------------------: |
|  [FrogRiverOne](https://app.codility.com/programmers/lessons/4-counting_elements/frog_river_one/)   |  [Link](lesson4-FrogRiverOne/test.js)  |
|    [MaxCounters](https://app.codility.com/programmers/lessons/4-counting_elements/max_counters/)    |  [Link](lesson4-MaxCounters/test.js)   |
| [MissingInteger](https://app.codility.com/programmers/lessons/4-counting_elements/missing_integer/) | [Link](lesson4-MissingInteger/test.js) |
|      [PermCheck](https://app.codility.com/programmers/lessons/4-counting_elements/perm_check/)      |   [Link](lesson4-PermCheck/test.js)    |

### Lesson 5 - Prefix Sums

|                                                 Task                                                 |                My solution                |
| :--------------------------------------------------------------------------------------------------: | :---------------------------------------: |
|          [CountDiv](https://app.codility.com/programmers/lessons/5-prefix_sums/count_div/)           |     [Link](lesson5-CountDiv/test.js)      |
| [GenomicRangeQuery](https://app.codility.com/programmers/lessons/5-prefix_sums/genomic_range_query/) | [Link](lesson5-GenomicRangeQuery/test.js) |
|   [MinAvgTwoSlice](https://app.codility.com/programmers/lessons/5-prefix_sums/min_avg_two_slice/)    |  [Link](lesson5-MinAvgTwoSlice/test.js)   |
|       [PassingCars](https://app.codility.com/programmers/lessons/5-prefix_sums/passing_cars/)        |    [Link](lesson5-PassingCars/test.js)    |

### Lesson 6 - Sorting

|                                     Task                                     |           My solution            |
| :--------------------------------------------------------------------------: | :------------------------------: |
| [Distinct](https://app.codility.com/programmers/lessons/6-sorting/distinct/) | [Link](lesson6-Distinct/test.js) | 
| [MaxProductOfThree](https://app.codility.com/programmers/lessons/6-sorting/max_product_of_three/) | [Link](lesson6-MaxProductOfThree/test.js)    |
| [NumberOfDiscIntersections](https://app.codility.com/programmers/lessons/6-sorting/number_of_disc_intersections/) | [Link](lesson6-NumberOfDiscIntersections/test.js)    |
| [Triangle](https://app.codility.com/programmers/lessons/6-sorting/triangle/) | [Link](lesson6-Triangle/test.js)    |
