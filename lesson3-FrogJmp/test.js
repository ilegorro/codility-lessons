const solution = function (X, Y, D) {
  const jumpsFloat = ( Y-X ) / D
  const jumpsFloor = Math.floor(jumpsFloat)
  if (jumpsFloat == jumpsFloor) {
    return jumpsFloor
  } else {
    return jumpsFloor + 1
  }
}

const X = +process.argv[2]
const Y = +process.argv[3]
const D = +process.argv[4]

console.log(solution(X, Y, D))