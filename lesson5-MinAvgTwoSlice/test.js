// It is sufficient to check only 2- and 3-dimension slices
// because every other slices consist of these 2- and 3-d slices
// and average of complex slices always equal of greater than 
// minimal average of atomic slices
const solution = function(A) {
  const lenA = A.length
  let minAvg = 10000
  let minAvgIdx = 0
  for (let i = 0; i < lenA - 2; i++) {
    const avgTwo = (A[i] + A[i+1]) / 2
    if (avgTwo < minAvg) {
      minAvg = avgTwo
      minAvgIdx = i
    }
    const avgThree = (A[i] + A[i+1] + A[i+2]) / 3
    if (avgThree < minAvg) {
      minAvg = avgThree
      minAvgIdx = i
    }
  }
  if ((A[lenA-2] + A[lenA-1]) / 2 < minAvg) {
    minAvgIdx = lenA - 2
  }
  return minAvgIdx
}

const A = [4, 2, 2, 5, 1, 5, 8]
console.log(solution(A))