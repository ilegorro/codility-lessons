const solution = function(A) {
  const tmpA = A.filter(el => el > 0).sort((a, b) => a - b)
  let prev = 0
  for (let i = 0; i < tmpA.length; i++) {
    if (tmpA[i] !== prev + 1 && tmpA[i] !== prev) {
      return prev + 1
    }
    prev = tmpA[i] 
  }
  return prev + 1
}

const A1 = [1, 3, 6, 4, 1, 2]
const A2 = [1, 2, 3]
const A3 = [-1, -2, -3]
console.log(solution(A1), solution(A2), solution(A3));