const solution = function (A) {
  if (A.length === 0) {
    return 1
  }
  A.sort((a, b) => a - b)
  if (A[0] === 2) {
    return 1
  }
  if (A[A.length - 1] !== A.length + 1) {
    return A.length + 1
  }
    for (let index = 1; index < A.length; index++) {
    if (A[index] !== A[index-1] + 1) {
      return A[index-1] + 1
    }
  }
}

const A = [2, 3, 1, 5, 9, 4, 8, 6, 10, 11, 12, 13, 7, 15]
console.log(solution(A))