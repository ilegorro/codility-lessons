const solution = function (A) {
  let minDif = null
  let sumAll = A.reduce((acc, val) => {
    acc += val
    return acc
  }, 0)
  let leftSums = {0: 0}
  let rightSums = {0: sumAll}
  
  for (let P = 1; P < A.length; P++){
    leftSums[P] = leftSums[P-1] + A[P-1]
    rightSums[P] = rightSums[P-1] - A[P-1]

    const dif = Math.abs(leftSums[P] - rightSums[P])
    minDif = minDif === null ? dif : Math.min(minDif, dif)
  }
  return minDif
}

const A = [3, 1, 2, 4, 3]
console.log(solution(A))