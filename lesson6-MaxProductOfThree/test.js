const solution = function (A) {

  const getMinEl = function (arr) {
    let minEl = arr[0]
    for (let i = 1; i < arr.length; i++) {
      const val = arr[i]
      if (val.element < minEl.element) {
        minEl = arr[i]
      }
    }
    return minEl
  }

  const getMaxEl = function (arr) {
    let maxEl = arr[0]
    for (let i = 1; i < arr.length; i++) {
      const val = arr[i]
      if (val.element > maxEl.element) {
        maxEl = arr[i]
      }
    }
    return maxEl
  }
  
  let max3 = []
  let minInMax3 = {}
  let min3 = []
  let maxInMin3 = {}
  let maxNeg3 = []
  let minInMaxNeg3 = {}

  A.forEach((element, idx) => {
    if (element >= 0) {
      if (max3.length < 3) {
        max3.push({ idx, element })
        minInMax3 = getMinEl(max3)
      } else {
        if (element > minInMax3.element) {
          max3 = max3.map(el => {
            return el.idx === minInMax3.idx ? { idx, element } : el
          })
          minInMax3 = getMinEl(max3)
        }
      }
    } else if (element < 0) {
      if (min3.length < 3) {
        min3.push({ idx, element })
        maxInMin3 = getMaxEl(min3)
      } else {
        if (element < maxInMin3.element) {
          min3 = min3.map((el) => {
            return el.idx === maxInMin3.idx ? { idx, element } : el
          })
          maxInMin3 = getMaxEl(min3)
        }
      }

      if (maxNeg3.length < 3) {
        maxNeg3.push({ idx, element })
        minInMaxNeg3 = getMinEl(maxNeg3)
      } else {
        if (element > minInMaxNeg3.element) {
          maxNeg3 = maxNeg3.map((el) => {
            return el.idx === minInMaxNeg3.idx ? { idx, element } : el
          })
          minInMaxNeg3 = getMinEl(maxNeg3)
        }
      }
      
    }
  })

  let nums = []
  if (max3.length === 0) {
    nums = [...maxNeg3]
  } else {
    nums = [...max3, ...min3]
  }
  const count = nums.length
  let maxProduct = Number.NEGATIVE_INFINITY
  for (let i = 0; i < count; i++) {
    for (let j = i+1; j < count; j++) {
      for (let k = j+1; k < count; k++) {
        maxProduct = Math.max(maxProduct, nums[i].element * nums[j].element * nums[k].element)
      }
    }
  }

  return maxProduct
}

const A = [-5, -6, -4, -7, -10]
console.log(solution(A))
