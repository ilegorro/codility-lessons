const solution = function(A, B, K) {
  let start
  for (let i = A; i < Math.max(A+K, B); i++) {
    if (i % K === 0) {
      start = i
      break
    }
  }
  if (start === undefined) {
    return 0
  } else {
    return Math.floor((B-start)/K) + 1
  }
}

const A = 6
const B = 11
const K = 2
console.log(solution(A, B, K))