const solution = function(X, A) {
  let leafs = new Set()
  //let arr = [...Array(X).keys()].map(i => i + 1)
  for (let i = 0; i < A.length; i++) {
    const element = A[i];
    if (element <= X) {
      leafs.add(element)
    }
    if (leafs.size === X) {
      return i
    }
  }
  return -1
}

const A = [1, 3, 1, 4, 2, 3, 7, 4]
const X = 5
console.log(solution(X, A))