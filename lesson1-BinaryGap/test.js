const binaryGap = function(num) {
  const binary = num.toString(2).split('1')
  let maxLength = 0
  console.log(binary)
  binary.forEach((el, idx) => {
    if (idx > 0 && idx < binary.length - 1) {
      maxLength = Math.max(maxLength, el.length)
    }
  })
  return maxLength
}
const number = +process.argv[2]
console.log(binaryGap(number))
