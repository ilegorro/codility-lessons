const solution = function(arr) {
  const map = arr.reduce((acc, val) => {
    acc[val] = acc.hasOwnProperty(val) ? acc[val] + 1 : 1
    return acc
  }, {})
  for (const key in map) {
    if (map[key] % 2 === 1) {
      return +key
    }
  }
  return undefined
}

const arr = [9, 3, 9, 3, 9, 7, 9]
console.log(solution(arr))