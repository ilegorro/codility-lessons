const solution = function(A) {
  let countPairs = []
  let count = 0
  let countOnes = 0
  A.forEach(element => {
    if (element === 0) {
      countPairs.push(countOnes)
    } else {
      countOnes++
    }
  })
  countPairs.forEach(element => {
    count += (countOnes - element)
  })
  return count > 1000000000 ? -1 : count
}

const A = [0, 1, 0, 1, 1]
console.log(solution(A))