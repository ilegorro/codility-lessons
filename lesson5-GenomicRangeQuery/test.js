const solution = function(S, P, Q) {
  const iMax = P.length
  const result = Array(iMax)
  for (let i = 0; i < iMax; i++) {
    const substr = S.substring(P[i], Q[i]+1)
    if (substr.includes('A')) {
      result[i] = 1
    } else if (substr.includes('C')) {
      result[i] = 2
    } else if (substr.includes('G')) {
      result[i] = 3
    } else {
      result[i] = 4
    }
  }
  return result
}

const S = 'CAGCCTA'
const P = [2, 5, 0]
const Q = [4, 5, 6]
console.log(solution(S, P, Q))