const solution = function(A) {
  A.sort((a, b) => a - b)
  let prev = A[0]
  if (prev !== 1) {
    return 0
  }
  for (let i = 1; i < A.length; i++) {
    if (A[i] !== prev + 1) {
      return 0
    }
    prev = A[i]
  }
  return 1
}

const A1 = [4, 1, 3, 2]
const A2 = [4, 1, 3]
const A3 = [4]
const A4 = [5, 5, 5]
const A5 = [1, 1]
console.log(solution(A1), solution(A2), solution(A3), solution(A4), solution(A5))