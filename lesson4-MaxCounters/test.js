const solution = function(N, A) {
  let counters = Array(N).fill(0)
  let maxValue = 0
  let maxAll = 0
  A.forEach(element => {
    if (element >= 1 && element <= N) {
      counters[element - 1] = Math.max(maxAll, counters[element - 1])
      maxValue = Math.max(maxValue, ++counters[element - 1])
    } else if (element === N + 1) {
      maxAll = maxValue
    }
  })
  return counters.map(el => Math.max(el, maxAll))
}

const N = 5
const A = [3, 4, 4, 6, 1, 4, 4]
console.log(solution(N, A))