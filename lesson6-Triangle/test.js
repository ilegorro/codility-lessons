const solution = function(A) {
  const sortedA = A.sort((a, b) => a - b)
  const len = A.length

  for (let i = 0; i < len; i++) {

    for (let j = i + 1; j < len; j++) {
      if (sortedA[j] >= sortedA[i] + sortedA[j + 1]) {
        break
      }

      for (let k = j + 1; k < len; k++) {
        if (sortedA[k] >= sortedA[i] + sortedA[j]) {
          break
        } else if ((sortedA[i] + sortedA[k]) > sortedA[j] && (sortedA[j] + sortedA[k]) > sortedA[i]){
          return 1
        }
      }
    }
  }
  return 0
}

const A = [1, 1, 2, 3, 5]
// const A = [10, 2, 6, 1, 8, 20]
// const A = [10, 50, 5, 1]

console.log(solution(A))
