const solution = function solution(A, K) {
  const N = A.length
  let mappedA = []
  let mapped = {}
  A.forEach((el, idx) => {
    const newIdx = (idx + K) % N
    mapped[newIdx] = el
  })
  for (let index = 0; index < N; index++) {
    mappedA.push(mapped[index])
  }
  return mappedA
}

const K = +process.argv[2]
const A = [3, 8, 9, 7, 6]
console.log(solution(A, K))