const solution = function(A) {
  let inters = 0

  const len = A.length
  const leftBounds = Array(len).fill(0)
  const rightBounds = Array(len).fill(0)
  for (let i = 0; i < len; i++) {
    if (i < A[i]) {
      // left boundary of the disk is miner than 0
      leftBounds[0]++
    } else {
      leftBounds[i - A[i]]++
    }
    if (i + A[i] >= len) {
      // right boundary of the disk is greater or equal than last right center
      rightBounds[len - 1]++
    } else {
      rightBounds[i + A[i]]++
    }
  }

  let activeDisks = 0
  for (let i = 0; i < len; i++) {
    const newDisksInters = (leftBounds[i] * (leftBounds[i] - 1)) / 2
    const activeDisksInters = activeDisks * leftBounds[i] 
    inters += activeDisksInters + newDisksInters
    activeDisks  = activeDisks + leftBounds[i] - rightBounds[i]
    if (inters > 10000000) {
      return -1
    }
  }

  return inters
}

const A = [1, 5, 2, 1, 4, 0]
console.log(solution(A))
